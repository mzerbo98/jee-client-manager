package beans;

public class Client {

	private int id;
	private String prenom, nom, telephone;
	
	public Client() {		
	}

	public Client(int id, String prenom, String nom, String telephone) {
		super();
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.telephone = telephone;
	}

	public Client(String prenom, String nom, String telephone) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.telephone = telephone;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
