package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import beans.Client;
import dao.ClientDAO;
import dao.DAOException;
import metier.forms.AjoutClientForm;
import metier.forms.UpdateClientForm;

/**
 * Servlet implementation class AjoutClient
 */
@WebServlet("/clients/*")
public class GestionClient extends HttpServlet
{
	private static final long				serialVersionUID		= 1L;
	private static final String				VUE_AJOUT_UTILISATEUR	= "/WEB-INF/ajoutClient.jsp";
	private static final String				VUE_LIST_UTILISATEUR	= "/WEB-INF/listeClient.jsp";

	public static final List<Client>	clients			= new ArrayList<Client>();
	private static final String				VUE_UPDATE_UTILISATEUR	= "/WEB-INF/modifierClient.jsp";

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		String requestedUrl = request.getRequestURI();
		if (requestedUrl.endsWith("/clients/add"))
		{
			getServletContext().getRequestDispatcher(VUE_AJOUT_UTILISATEUR)
					.forward(request, response);
		}
		else if (requestedUrl.endsWith("/clients/list"))
		{
			try {
				request.setAttribute("clients", ClientDAO.getList());
			} catch (DAOException e) {
				e.printStackTrace();
			}
			getServletContext().getRequestDispatcher(VUE_LIST_UTILISATEUR)
					.forward(request, response);
		}
		else if (requestedUrl.endsWith("/clients/update"))
		{
			String id = request.getParameter("id");
			if (id!=null) {
				request.setAttribute("id",id);
				List<Client> users = new ArrayList<Client>();
				Client user = null;
				try {
					users = ClientDAO.getList();
				} catch (DAOException e) {
					e.printStackTrace();
				}
				for (Client c : users) {
					if (c.getId() == Integer.valueOf(id)) {
						user = c;
					}
				}
				if (user!=null) {
					request.setAttribute("utilisateur", user);
				}
				getServletContext().getRequestDispatcher(VUE_UPDATE_UTILISATEUR)
						.forward(request, response);
			}
			try {
				request.setAttribute("clients", ClientDAO.getList());
			} catch (DAOException e) {
				e.printStackTrace();
			}
			getServletContext().getRequestDispatcher(VUE_LIST_UTILISATEUR)
					.forward(request, response);
			
		}
		else if (requestedUrl.endsWith("/clients/delete"))
		{
			response.sendRedirect("list");
		}
		else
		{
			response.sendRedirect(request.getContextPath());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		request.setCharacterEncoding("utf-8");
		String requestedUrl = request.getRequestURI();

		if (requestedUrl.endsWith("/clients/add"))
		{
			AjoutClientForm form = new AjoutClientForm(request);
			Client utilisateur = form.getClient();

			if (form.isValid())
			{
				try {
					ClientDAO.ajouter(utilisateur);
				} catch (DAOException e) {
					e.printStackTrace();
				}
			}

			request.setAttribute("utilisateur", utilisateur);
			request.setAttribute("messageErreurs", form.getMessageErreurs());
			request.setAttribute("statusMessage", form.getStatusMessage());

			getServletContext().getRequestDispatcher(VUE_AJOUT_UTILISATEUR)
					.forward(request, response);
		} else {
			if (requestedUrl.endsWith("/clients/update"))
			{
				UpdateClientForm form = new UpdateClientForm(request);
				Client utilisateur = form.getClient();

				if (form.isValid())
				{
					try {
						ClientDAO.modifier(utilisateur);
					} catch (DAOException e) {
						e.printStackTrace();
					}
				}

				request.setAttribute("utilisateur", utilisateur);
				request.setAttribute("messageErreurs", form.getMessageErreurs());
				request.setAttribute("statusMessage", form.getStatusMessage());

				try {
					request.setAttribute("clients", ClientDAO.getList());
				} catch (DAOException e) {
					e.printStackTrace();
				}
				getServletContext().getRequestDispatcher(VUE_LIST_UTILISATEUR)
						.forward(request, response);
			}
		}
	}
	
	public static List<Client> getClients()
	{
		return clients;
	}
}
