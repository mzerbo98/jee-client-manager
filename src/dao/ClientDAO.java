package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Client;

public class ClientDAO {
	private static final String	AJOUT_UTILISATEUR_SQL	= "INSERT INTO Client VALUES(0, ?, ?, ?)";
	private static final String	SELECT_UTILISATEUR_SQL	= "SELECT * FROM Client";
	private static final String	UPDATE_UTILISATEUR_SQL	= "UPDATE Client SET prenom=?, nom=?, telephone=? where id = ?";
	private static final String	DELETE_UTILISATEUR_SQL	= "DELETE FROM Client where nom = ?";

	public static void ajouter(Client client) throws DAOException
	{
		Connection connexion = DatabaseManager.getConnection();
		try
		{
			PreparedStatement statement = connexion
					.prepareStatement(AJOUT_UTILISATEUR_SQL);
			statement.setString(1, client.getNom());
			statement.setString(2, client.getPrenom());
			statement.setString(3, client.getTelephone());
			statement.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new DAOException("Client non ajout�");
		}
	}
	
	public static void supprimer(Client client) throws DAOException
	{
		Connection connexion = DatabaseManager.getConnection();
		try
		{
			PreparedStatement statement = connexion
					.prepareStatement(DELETE_UTILISATEUR_SQL);
			statement.setInt(1, client.getId());
			statement.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new DAOException("Client non ajout�");
		}
	}
	
	public static void modifier(Client client) throws DAOException
	{
		Connection connexion = DatabaseManager.getConnection();
		try
		{
			PreparedStatement statement = connexion
					.prepareStatement(UPDATE_UTILISATEUR_SQL);
			statement.setString(1, client.getPrenom());
			statement.setString(2, client.getNom());
			statement.setString(3, client.getTelephone());
			statement.setInt(4, client.getId());
			statement.executeUpdate();
		}
		catch (SQLException e)
		{
			throw new DAOException("Client non ajout�");
		}
	}

	public static List<Client> getList() throws DAOException
	{
		Connection connexion = DatabaseManager.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		ArrayList<Client> clients = null;
		try
		{
			int id;
			clients = new ArrayList<Client>();
			String nom, prenom, telephone;
			statement = connexion.createStatement();
			resultSet = statement.executeQuery(SELECT_UTILISATEUR_SQL);
			while (resultSet.next())
			{
				id = resultSet.getInt(1);
				nom = resultSet.getString(2);
				prenom = resultSet.getString(3);
				telephone = resultSet.getString(4);
				clients.add(new Client(id, nom, prenom, telephone));
			}
		}
		catch (SQLException e)
		{
			throw new DAOException(e.getMessage());
		}
		return clients;
	}
}
