package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseManager {
	private static final String	DB_URL		= "jdbc:mysql://localhost/javaee";
	private static final String	DB_USER		= "jee";
	private static final String	DB_PASSWORD	= "jee";

	private static Connection	connexion;

	private DatabaseManager()
	{
	}

	public static Connection getConnection() throws DAOException
	{
		if (connexion == null)
		{
			try
			{
				Class.forName("com.mysql.jdbc.Driver");
				Connection connexion = DriverManager.getConnection(DB_URL,
						DB_USER, DB_PASSWORD);
				return connexion;
			}
			catch (ClassNotFoundException e)
			{
				throw new DAOException("Erreur de chargement du pilote");
			}
			catch (SQLException e)
			{
				throw new DAOException("Erreur d'acc�s �la base de donn�es : "
						+ e.getMessage());
			}
		}
		return connexion;
	}
}
