package metier.forms;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;


public class FormUtils {
	
	private HttpServletRequest	request;
	private Map<String, String>	messageErreurs;
	
	
	public FormUtils(HttpServletRequest request,Map<String, String>	messageErreurs) {
		super();
		this.request = request;
		this.messageErreurs = messageErreurs;
	}

	public void validerChamps(String... champs)
	{
		for (String champ : champs)
		{
			if (getParamater(champ) == null)
			{
				messageErreurs.put(champ, "Vous devez renseigner ce champ");
			}
		}
	}

	public String getParamater(String parametre)
	{
		String valeur = request.getParameter(parametre);
		valeur = valeur == null || valeur.trim().isEmpty() ? null
				: valeur.trim();
		return valeur;
	}

	public boolean isValid()
	{
		return messageErreurs.isEmpty();
	}


}
