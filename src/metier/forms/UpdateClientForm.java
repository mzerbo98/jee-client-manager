package metier.forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import beans.Client;

public class UpdateClientForm {
	private static final String	CHAMP_ID			= "id";
	private static final String	CHAMP_NOM			= "nom";
	private static final String	CHAMP_PRENOM		= "prenom";
	private static final String	CHAMP_TELEPHONE			= "telephone";

	private Client	client;
	private String	statusMessage;
	private FormUtils utils;
	private Map<String, String>	messageErreurs = new HashMap<String, String>();

	public UpdateClientForm(HttpServletRequest request)
	{
		utils = new FormUtils(request,messageErreurs);

		String id = utils.getParamater(CHAMP_ID);
		String nom = utils.getParamater(CHAMP_NOM);
		String prenom = utils.getParamater(CHAMP_PRENOM);
		String telephone = utils.getParamater(CHAMP_TELEPHONE);
		client = new Client(Integer.valueOf(id), nom, prenom, telephone);

		utils.validerChamps(CHAMP_NOM, CHAMP_PRENOM, CHAMP_TELEPHONE);
		if (messageErreurs.isEmpty())
		{
			statusMessage = "Mise a jour effectu� avec succ�s";
		}
		else
		{
			statusMessage = "Echec de la mise � jour du client";
		}
	}

	public Client getClient() {
		return client;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public Map<String, String> getMessageErreurs() {
		return messageErreurs;
	}
	
	public boolean isValid()
	{
		return messageErreurs.isEmpty();
	}
	
}
