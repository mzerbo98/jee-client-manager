package metier.forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import beans.Client;
import dao.ClientDAO;
import dao.DAOException;

public class AjoutClientForm {
	private static final String	CHAMP_NOM			= "nom";
	private static final String	CHAMP_PRENOM		= "prenom";
	private static final String	CHAMP_TELEPHONE			= "telephone";

	private Client	client;
	private String	statusMessage;
	private FormUtils utils;
	private Map<String, String>	messageErreurs = new HashMap<String, String>();

	public AjoutClientForm(HttpServletRequest request)
	{
		utils = new FormUtils(request,messageErreurs);

		String nom = utils.getParamater(CHAMP_NOM);
		String prenom = utils.getParamater(CHAMP_PRENOM);
		String telephone = utils.getParamater(CHAMP_TELEPHONE);

		client = new Client(nom, prenom, telephone);

		utils.validerChamps(CHAMP_NOM, CHAMP_PRENOM, CHAMP_TELEPHONE);
		if (messageErreurs.isEmpty())
		{
			statusMessage = "Ajout effectu� avec succ�s";
		}
		else
		{
			statusMessage = "Echec de l'ajout de l'utilisateur";
		}
	}

	public Client getClient() {
		return client;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public Map<String, String> getMessageErreurs() {
		return messageErreurs;
	}
	
	public boolean isValid()
	{
		return messageErreurs.isEmpty();
	}
	
}
