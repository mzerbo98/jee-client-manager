package metier.forms;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import beans.Client;

public class AuthenticationForm {

	private static final String	CHAMP_LOGIN		= "login";
	private static final String	CHAMP_PASSWORD	= "password";
	private HttpServletRequest	request;
	private String				login;
	private FormUtils utils;

	public AuthenticationForm(HttpServletRequest request)
	{
		this.request = request;
		utils = new FormUtils(request, null);
		
	}

	public Client connect()
	{
		
		login = utils.getParamater(CHAMP_LOGIN);
		String password = utils.getParamater(CHAMP_PASSWORD);
		
		if ("admin".equals(login) && "passer".equals(password))
		{
			HttpSession session = request.getSession();
			Client utilisateur = new Client(0, "ADMIN", "admin",
					"77228147");
			session.setAttribute("admin", utilisateur);
			return utilisateur;
		}
		else
		{
			return null;
		}
	}

	public String getLogin()
	{
		return login;
	}
}
