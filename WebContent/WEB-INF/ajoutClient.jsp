<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ajout d'un client</title>
	<link rel="stylesheet" href="<c:url value='/style.css'/>">
</head>
<body>
	<c:import url="inc/entete.jsp"/>
	<c:import url="inc/menu.jsp"/>
	<form method="post" action="add">
		<fieldset>
			<legend>Ajout d'un client</legend>
			<label>Nom :</label>
			<input type="text" name="nom" value="${ requestScope.utilisateur.nom }">
			<span>${ messageErreurs.nom }</span><br>
			<label>Prénom :</label>
			<input type="text" name="prenom" value="${ requestScope.utilisateur.prenom }">
			<span>${ messageErreurs.prenom }</span><br>
			<label>N° Telephone :</label>
			<input type="text" name="telephone" value="${ requestScope.utilisateur.telephone }">
			<span>${ messageErreurs.telephone }</span><br>
			<input type="submit" value="Ajouter">
			<span class="${ empty messageErreurs ? 'succes' : 'erreur'}">${ statusMessage }</span>
		</fieldset>
	</form>
</body>
</html>