<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Accueil</title>
	<link rel="stylesheet" href="<c:url value='/style.css'/>">
</head>
<body>
	<c:import url="inc/entete.jsp"/>
	<c:import url="inc/menu.jsp"/>
	<h1>Page d'accueil</h1>
	<p>Bienvenue dans notre application de gestion des clients</p>
	<p>Vous pouvez choisir une option dans le menu proposé</p>
</body>
</html>