<%@page import="beans.Client"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste des clients</title>
<link rel="stylesheet" href="<c:url value='/style.css'/>">
</head>
<body>
	<c:import url="inc/entete.jsp"/>
	<c:import url="inc/menu.jsp"/>
	<c:choose>
		<c:when test="${ empty clients }">
			<p>La liste des clients est vide</p>
		</c:when>
		<c:otherwise>
			<table border="1" cellspacing="0">
				<tr>
					<th>Nom</th>
					<th>Prénom</th>
					<th>Telephone</th>
				</tr>
				<c:forEach items="${ clients }" var="client">
					<tr>
						<td><c:out value="${ client.nom }"/></td>
						<td><c:out value="${ client.prenom }"/></td>
						<td><c:out value="${ client.telephone }"/></td>
						<td> <a href="<c:url value='/clients/update?id=${ client.id }'/>">Modifier</a> </td>
					</tr>
				</c:forEach>
			</table>
		</c:otherwise>
	</c:choose>
	
</body>
</html>